<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function index(){
        return view('admin/admin');
    }

    public function datatable(){
        return view('admin/admin-datatable');
    }
}
