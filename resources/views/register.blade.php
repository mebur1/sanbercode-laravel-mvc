<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <form action="{{url('welcome')}}" method="post">
        @csrf
        <p>First name:</p>
        <input type="text" id="fname" name="fname" value=""><br>

        <p>Last name:</p>
        <input type="text" id="lname" name="lname" value=""><br>

        <p>Gender:</p>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br>

        <p>Nationality:</p>
        <select name="nationality" id="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="american">American</option>
            <option value="britania">Britania</option>
            <option value="wakanda">Wakanda</option>
        </select>
        <br>

        <p>Language Spoken:</p>
        <input type="checkbox" name="language_spoken" value="bahasa_indonesia">
        <label for="bahasa_indonesia"> Bahasa Indonesia</label><br>
        <input type="checkbox" name="language_spoken" value="english">
        <label for="english"> English </label><br>
        <input type="checkbox" name="language_spoken" value="other">
        <label for="other"> Other </label>
        <br>

        <p>Bio:</p>
        <textarea id="w3review" name="w3review" rows="7" cols="20">
        </textarea>
        <br>
        <input type="submit" value="Sign Up">
    </form>
</body>

</html>